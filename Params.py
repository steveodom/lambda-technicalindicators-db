from decimal import Decimal 
import json
from boto3.dynamodb.conditions import Key, Attr

class Params(object):
    def __init__(self, event):
        self.filters = json.loads(event.get('q'))
        self.period = event.get('period') or "daily"

        self.direction_filters = []
        self.indicator_filters = []

    def build_key_condition(self):
        base = Key('period').eq(self.period)
        use_direction_key = False
        
        if len(self.direction_filters) == 2:
            use_direction_key = True
            # directions = [filter['val'] for filter in self.direction_filters]
            # base = base & Key('Direction').in(directions)

        elif len(self.direction_filters) == 1:
            use_direction_key = True
            direction = self.direction_filters[0]
            base = base & Key('Direction').eq(direction['val'])
        return base

    def separate_types_of_params(self):
        for filter in self.filters:
            if filter['id'] == "direction":
                self.direction_filters.append(filter)
            else:
                self.indicator_filters.append(filter)

    def build_filter_expression(self):
        args = None
        for filter in self.indicator_filters:
            exp = Key(filter['id']).eq(filter['val'])
            if args is None:
                args = exp
            else:
                args = args & exp
        return args

    def build(self):
        self.separate_types_of_params()
        key_conditions = self.build_key_condition()
        filter_expression = self.build_filter_expression()

        return key_conditions, filter_expression
