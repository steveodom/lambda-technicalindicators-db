# -*- coding: utf-8 -*-
from boto3.dynamodb.conditions import Key, Attr
from Query import Query
from Params import Params

def handler(event, context):
    table = event.get('table') or "TechnicalHistoryTable"
    projection_expression = event.get('projection_expression') or "ticker"
    key_conditions, filter_expressions = Params(event).build()
    nextToken = event.get('nextToken')
    limit = event.get('limit') or 200
    return Query("us-east-1", table).paginated_query(key_conditions, filter_expressions, projection_expression, limit, nextToken)
