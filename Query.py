import boto3
from boto3.dynamodb.conditions import Key, Attr

class Query(object):
    def __init__(self, region_name="us-east-1", table_name=""):
        self.conn = boto3.resource('dynamodb', region_name=region_name)
    
        if table_name:
          self.table_name = table_name
          self.table = self.conn.Table(table_name)
    
    # TODO: must be a better way to DRY these two queries up
    # NOTE: I found leaving off the Limit=n on the query results in fewer scans of the table
    def query_without_start_key(self, key_conditions, filter_expressions, projection_expression, limit):
        return self.table.query(
          IndexName="period-Direction-index",
          KeyConditionExpression=key_conditions,
          FilterExpression=filter_expressions,
          ProjectionExpression=projection_expression
        )

    def query_with_start_key(self, key_conditions, filter_expressions, projection_expression, limit, next_token):
        return self.table.query(
          IndexName="period-Direction-index",
          KeyConditionExpression=key_conditions,
          FilterExpression=filter_expressions,
          ProjectionExpression=projection_expression,
          ExclusiveStartKey=next_token
          
        )

    def paginated_query(self, key_conditions, filter_expressions, projection_expression, limit, next_token):
        if next_token:
          response = self.query_with_start_key(key_conditions, filter_expressions, projection_expression, limit, next_token)
        else:
          response = self.query_without_start_key(key_conditions, filter_expressions, projection_expression, limit)
        
        data = response['Items']
        while 'LastEvaluatedKey' in response and len(data) < limit:
          response = self.query_with_start_key(key_conditions, filter_expressions, projection_expression, limit, response['LastEvaluatedKey'])
          data.extend(response['Items'])
        if 'LastEvaluatedKey' in response:
          return {"data": data, "len": len(data), "LastEvaluatedKey": response['LastEvaluatedKey']}
        else:
          return {"data": data, "len": len(data)}
        